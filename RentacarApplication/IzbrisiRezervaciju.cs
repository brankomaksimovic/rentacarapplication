﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentacarApplication
{
    public partial class IzbrisiRezervaciju : Form
    {
        public IzbrisiRezervaciju()
        {
            InitializeComponent();
            popuniCombobox();

        }
        public void popuniCombobox()
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("select a.naziv_automobila+''+k.ime as rezervacija,r.idrezervacija from Rezervacija as r inner join Automobili as a on a.idautomobili=r.idautomobil inner join Korisnik as k on k.idkorisnik=r.idkorisnik",konekcija);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            comboBox1.DataSource = tabela;
            comboBox1.DisplayMember = "rezervacija";
            comboBox1.ValueMember = "idrezervacija";
            konekcija.Close();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("delete from Rezervacija where idrezervacija=@rezervacija", konekcija);
            komanda.Parameters.AddWithValue("@rezervacija", comboBox1.SelectedValue);
            komanda.ExecuteNonQuery();
            MessageBox.Show("Uspesno ste obrisali rezervaciju!");
            konekcija.Close();
            popuniCombobox();


        }
    }
}
