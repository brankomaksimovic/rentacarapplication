﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentacarApplication
{
    public partial class RezervacijaKlijenta : Form
    {
        public int idklijenta;

        public RezervacijaKlijenta(int idklijenta)

        {
            InitializeComponent();
            this.idklijenta = idklijenta;
            popunigrid();
        }
        public void popunigrid()
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("sp_getRezervacijabyKlijent",konekcija);
            komanda.CommandType = CommandType.StoredProcedure;
            komanda.Parameters.AddWithValue("@klijent", this.idklijenta);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            dataGridView1.DataSource = tabela;

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
