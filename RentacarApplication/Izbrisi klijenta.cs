﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentacarApplication
{
    public partial class Izbrisi_klijenta : Form
    {
        public Izbrisi_klijenta()
        {
            InitializeComponent();
            popuniCombobox();
        }

        public void popuniCombobox()
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("select ime+''+prezime as klijent,idkorisnik from Korisnik", konekcija);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            comboBox1.DataSource = tabela;
            comboBox1.DisplayMember = "klijent";
            comboBox1.ValueMember = "idkorisnik";
            konekcija.Close();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("delete from Korisnik where idkorisnik=@korisnik", konekcija);
            komanda.Parameters.AddWithValue("@korisnik", comboBox1.SelectedValue);
            komanda.ExecuteNonQuery();
            MessageBox.Show("Uspesno ste obrisali klijenta!");
            konekcija.Close();
            popuniCombobox();

        }
    }
}
