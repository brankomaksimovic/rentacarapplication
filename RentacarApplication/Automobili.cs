﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentacarApplication
{
    public partial class Automobili : Form
    {
        public Automobili()
        {
            InitializeComponent();
            popuniComboBox();

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        private void popuniComboBox()
        {
            SqlConnection kon = new SqlConnection(Konekcija.kon);
            kon.Open();
            SqlCommand komanda = new SqlCommand("select naziv,idmarka from Marka",kon);
            DataTable tabela = new DataTable();
            SqlDataAdapter adap = new SqlDataAdapter(komanda);
            adap.Fill(tabela);
            comboBox1.DataSource = tabela;
            comboBox1.ValueMember = "idmarka";
            comboBox1.DisplayMember = "naziv";
            kon.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection kon = new SqlConnection(Konekcija.kon);
            kon.Open();
            SqlCommand komanda = new SqlCommand("insert into Automobili(naziv_automobila,idmarka,broj_konja,boja,registracija) values(@naziv,@marka,@broj_konja,@boja,@registracija)", kon);
            komanda.Parameters.AddWithValue("@naziv", textBox1.Text);
            komanda.Parameters.AddWithValue("@marka",comboBox1.SelectedValue);
            komanda.Parameters.AddWithValue("@broj_konja", textBox2.Text);
            komanda.Parameters.AddWithValue("@boja", textBox3.Text);
            komanda.Parameters.AddWithValue("@registracija", textBox4.Text);
            komanda.ExecuteNonQuery();
            MessageBox.Show("Dodali ste novi automobil.");
            kon.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            MessageBox.Show("Odustali ste od dodavanja automobila!");
        }
    }
}
