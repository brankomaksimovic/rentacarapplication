﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentacarApplication
{
    public partial class izmenaVozila : Form
    {
        public izmenaVozila()
        {
            InitializeComponent();
            popuniCombobox();
            popuniMarke();


        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = (int)comboBox2.SelectedValue;
            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("select * from Automobili where idautomobili=@idautomobili",konekcija);
            komanda.Parameters.AddWithValue("@idautomobili", id);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            foreach(DataRow red in tabela.Rows)
            {
                textBox1.Text = red["naziv_automobila"].ToString();
                comboBox1.SelectedValue =(int) red["idmarka"];
                textBox2.Text = red["broj_konja"].ToString();
                textBox3.Text = red["boja"].ToString();
                textBox4.Text = red["registracija"].ToString();


            }
            konekcija.Close();



        }
        private void popuniCombobox()
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("select a.idautomobili,a.naziv_automobila+''+m.naziv as naziv_automobila from Automobili as a inner join Marka as m on m.idmarka=a.idmarka", konekcija);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            konekcija.Close();
            comboBox2.DataSource = tabela;
            comboBox2.DisplayMember = "naziv_automobila";
            comboBox2.ValueMember = "idautomobili";


        }
        private void popuniMarke()
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("select * from Marka", konekcija);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            konekcija.Close();
            comboBox1.DataSource = tabela;
            comboBox1.DisplayMember = "naziv";
            comboBox1.ValueMember = "idmarka";

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("update Automobili set naziv_automobila=@naziv_automobila,idmarka=@idmarka,broj_konja=@broj_konja,boja=@boja,registracija=@registracija where idautomobili=@id", konekcija);
            komanda.Parameters.AddWithValue("@naziv_automobila", textBox1.Text);
            komanda.Parameters.AddWithValue("@broj_konja", textBox2.Text);
            komanda.Parameters.AddWithValue("@boja", textBox3.Text);
            komanda.Parameters.AddWithValue("@registracija", textBox4.Text);
            komanda.Parameters.AddWithValue("@idmarka", comboBox1.SelectedValue);
            komanda.Parameters.AddWithValue("@id", comboBox2.SelectedValue);
            komanda.ExecuteNonQuery();
            konekcija.Close();
            MessageBox.Show("Uspesno ste izmenili parametar vozila!");

        }
    }
}
