﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentacarApplication
{
    public partial class IzbrisiVozilo : Form
    {
        public IzbrisiVozilo()
        {
            InitializeComponent();
            popuniCombobox();

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void obrisiVozilo()
        {
            

        }

        public void popuniCombobox()
        {

            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("select a.idautomobili, a.naziv_automobila+''+m.naziv as automobil,m.idmarka from Automobili as a inner join Marka as m on a.idmarka = m.idmarka", konekcija);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            comboBox1.DataSource = tabela;
            comboBox1.DisplayMember = "automobil";
            comboBox1.ValueMember = "idautomobili";
            konekcija.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("delete from Automobili where idautomobili=@automobil", konekcija);
            komanda.Parameters.AddWithValue("@automobil", comboBox1.SelectedValue);
            komanda.ExecuteNonQuery();
            MessageBox.Show("Uspesno ste obrisali automobil!");
            konekcija.Close();
            popuniCombobox();
        }
    }
}
