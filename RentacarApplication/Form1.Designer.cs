﻿namespace RentacarApplication
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabovi = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.klijentiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodavanjeKlijentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izmenaKlijentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.brisanjeKlijenataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vozilaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodavanjeVozilaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izmenaVozilaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.brisanjeVozilaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.markaAutomobilaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodavanjeMarkiAutomobilaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izmenaMarkiAutomobilaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izbrisiMarkuAutomobilaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rezervacijeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajRezervacijuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.obrisiRezervacijuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.tabovi.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.SuspendLayout();
            // 
            // tabovi
            // 
            this.tabovi.Controls.Add(this.tabPage1);
            this.tabovi.Controls.Add(this.tabPage2);
            this.tabovi.Controls.Add(this.tabPage3);
            this.tabovi.Controls.Add(this.tabPage4);
            this.tabovi.Location = new System.Drawing.Point(1, 29);
            this.tabovi.Name = "tabovi";
            this.tabovi.SelectedIndex = 0;
            this.tabovi.Size = new System.Drawing.Size(798, 421);
            this.tabovi.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView5);
            this.tabPage1.Controls.Add(this.dataGridView4);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(790, 395);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "pocetna strana";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.dataGridView3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(790, 395);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Rezervacije";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(692, 67);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "Osvezi grid";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(-121, -4);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(764, 399);
            this.dataGridView3.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Controls.Add(this.dataGridView1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(790, 395);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Klijenti";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(712, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Osvezi prikaz";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(706, 392);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.button1);
            this.tabPage4.Controls.Add(this.dataGridView2);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(790, 395);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Vozila";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(712, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Osvezi grid";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(0, 0);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(706, 392);
            this.dataGridView2.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.klijentiToolStripMenuItem,
            this.vozilaToolStripMenuItem,
            this.markaAutomobilaToolStripMenuItem,
            this.rezervacijeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(845, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // klijentiToolStripMenuItem
            // 
            this.klijentiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodavanjeKlijentaToolStripMenuItem,
            this.izmenaKlijentaToolStripMenuItem,
            this.brisanjeKlijenataToolStripMenuItem});
            this.klijentiToolStripMenuItem.Name = "klijentiToolStripMenuItem";
            this.klijentiToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.klijentiToolStripMenuItem.Text = "Klijenti";
            // 
            // dodavanjeKlijentaToolStripMenuItem
            // 
            this.dodavanjeKlijentaToolStripMenuItem.Name = "dodavanjeKlijentaToolStripMenuItem";
            this.dodavanjeKlijentaToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.dodavanjeKlijentaToolStripMenuItem.Text = "Dodavanje klijenta";
            this.dodavanjeKlijentaToolStripMenuItem.Click += new System.EventHandler(this.dodavanjeKlijentaToolStripMenuItem_Click);
            // 
            // izmenaKlijentaToolStripMenuItem
            // 
            this.izmenaKlijentaToolStripMenuItem.Name = "izmenaKlijentaToolStripMenuItem";
            this.izmenaKlijentaToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.izmenaKlijentaToolStripMenuItem.Text = "Izmena klijenta";
            this.izmenaKlijentaToolStripMenuItem.Click += new System.EventHandler(this.izmenaKlijentaToolStripMenuItem_Click);
            // 
            // brisanjeKlijenataToolStripMenuItem
            // 
            this.brisanjeKlijenataToolStripMenuItem.Name = "brisanjeKlijenataToolStripMenuItem";
            this.brisanjeKlijenataToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.brisanjeKlijenataToolStripMenuItem.Text = "Brisanje klijenata";
            this.brisanjeKlijenataToolStripMenuItem.Click += new System.EventHandler(this.brisanjeKlijenataToolStripMenuItem_Click);
            // 
            // vozilaToolStripMenuItem
            // 
            this.vozilaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodavanjeVozilaToolStripMenuItem,
            this.izmenaVozilaToolStripMenuItem,
            this.brisanjeVozilaToolStripMenuItem});
            this.vozilaToolStripMenuItem.Name = "vozilaToolStripMenuItem";
            this.vozilaToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.vozilaToolStripMenuItem.Text = "Vozila";
            // 
            // dodavanjeVozilaToolStripMenuItem
            // 
            this.dodavanjeVozilaToolStripMenuItem.Name = "dodavanjeVozilaToolStripMenuItem";
            this.dodavanjeVozilaToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.dodavanjeVozilaToolStripMenuItem.Text = "Dodavanje vozila";
            this.dodavanjeVozilaToolStripMenuItem.Click += new System.EventHandler(this.dodavanjeVozilaToolStripMenuItem_Click);
            // 
            // izmenaVozilaToolStripMenuItem
            // 
            this.izmenaVozilaToolStripMenuItem.Name = "izmenaVozilaToolStripMenuItem";
            this.izmenaVozilaToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.izmenaVozilaToolStripMenuItem.Text = "Izmena vozila";
            this.izmenaVozilaToolStripMenuItem.Click += new System.EventHandler(this.izmenaVozilaToolStripMenuItem_Click);
            // 
            // brisanjeVozilaToolStripMenuItem
            // 
            this.brisanjeVozilaToolStripMenuItem.Name = "brisanjeVozilaToolStripMenuItem";
            this.brisanjeVozilaToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.brisanjeVozilaToolStripMenuItem.Text = "Brisanje vozila";
            this.brisanjeVozilaToolStripMenuItem.Click += new System.EventHandler(this.brisanjeVozilaToolStripMenuItem_Click);
            // 
            // markaAutomobilaToolStripMenuItem
            // 
            this.markaAutomobilaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodavanjeMarkiAutomobilaToolStripMenuItem,
            this.izmenaMarkiAutomobilaToolStripMenuItem,
            this.izbrisiMarkuAutomobilaToolStripMenuItem});
            this.markaAutomobilaToolStripMenuItem.Name = "markaAutomobilaToolStripMenuItem";
            this.markaAutomobilaToolStripMenuItem.Size = new System.Drawing.Size(116, 20);
            this.markaAutomobilaToolStripMenuItem.Text = "Marka automobila";
            // 
            // dodavanjeMarkiAutomobilaToolStripMenuItem
            // 
            this.dodavanjeMarkiAutomobilaToolStripMenuItem.Name = "dodavanjeMarkiAutomobilaToolStripMenuItem";
            this.dodavanjeMarkiAutomobilaToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.dodavanjeMarkiAutomobilaToolStripMenuItem.Text = "Dodavanje marki automobila";
            this.dodavanjeMarkiAutomobilaToolStripMenuItem.Click += new System.EventHandler(this.dodavanjeMarkiAutomobilaToolStripMenuItem_Click);
            // 
            // izmenaMarkiAutomobilaToolStripMenuItem
            // 
            this.izmenaMarkiAutomobilaToolStripMenuItem.Name = "izmenaMarkiAutomobilaToolStripMenuItem";
            this.izmenaMarkiAutomobilaToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.izmenaMarkiAutomobilaToolStripMenuItem.Text = "Izmena marki automobila";
            this.izmenaMarkiAutomobilaToolStripMenuItem.Click += new System.EventHandler(this.izmenaMarkiAutomobilaToolStripMenuItem_Click);
            // 
            // izbrisiMarkuAutomobilaToolStripMenuItem
            // 
            this.izbrisiMarkuAutomobilaToolStripMenuItem.Name = "izbrisiMarkuAutomobilaToolStripMenuItem";
            this.izbrisiMarkuAutomobilaToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.izbrisiMarkuAutomobilaToolStripMenuItem.Text = "Izbrisi marku automobila";
            this.izbrisiMarkuAutomobilaToolStripMenuItem.Click += new System.EventHandler(this.izbrisiMarkuAutomobilaToolStripMenuItem_Click);
            // 
            // rezervacijeToolStripMenuItem
            // 
            this.rezervacijeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajRezervacijuToolStripMenuItem,
            this.obrisiRezervacijuToolStripMenuItem});
            this.rezervacijeToolStripMenuItem.Name = "rezervacijeToolStripMenuItem";
            this.rezervacijeToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.rezervacijeToolStripMenuItem.Text = "Rezervacije";
            // 
            // dodajRezervacijuToolStripMenuItem
            // 
            this.dodajRezervacijuToolStripMenuItem.Name = "dodajRezervacijuToolStripMenuItem";
            this.dodajRezervacijuToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.dodajRezervacijuToolStripMenuItem.Text = "Dodaj rezervaciju";
            this.dodajRezervacijuToolStripMenuItem.Click += new System.EventHandler(this.dodajRezervacijuToolStripMenuItem_Click);
            // 
            // obrisiRezervacijuToolStripMenuItem
            // 
            this.obrisiRezervacijuToolStripMenuItem.Name = "obrisiRezervacijuToolStripMenuItem";
            this.obrisiRezervacijuToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.obrisiRezervacijuToolStripMenuItem.Text = "Obrisi rezervaciju";
            this.obrisiRezervacijuToolStripMenuItem.Click += new System.EventHandler(this.obrisiRezervacijuToolStripMenuItem_Click);
            // 
            // dataGridView4
            // 
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(3, 0);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(790, 169);
            this.dataGridView4.TabIndex = 0;
            // 
            // dataGridView5
            // 
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Location = new System.Drawing.Point(3, 175);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.Size = new System.Drawing.Size(790, 234);
            this.dataGridView5.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 450);
            this.Controls.Add(this.tabovi);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabovi.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabovi;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem klijentiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodavanjeKlijentaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izmenaKlijentaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vozilaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodavanjeVozilaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izmenaVozilaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markaAutomobilaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodavanjeMarkiAutomobilaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izmenaMarkiAutomobilaToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripMenuItem rezervacijeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajRezervacijuToolStripMenuItem;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ToolStripMenuItem obrisiRezervacijuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem brisanjeKlijenataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem brisanjeVozilaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izbrisiMarkuAutomobilaToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.DataGridView dataGridView5;
    }
}

