﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentacarApplication
{
    public partial class IzmenaKlijenta : Form
    {
        public IzmenaKlijenta()
        {
            InitializeComponent();
            popunaKlijentima();
            popuniGradove();


        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id =(int) comboBox2.SelectedValue;
           
            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("select * from Korisnik where idkorisnik=@idkorisnik", konekcija);
            komanda.Parameters.AddWithValue("@idkorisnik", id);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            foreach(DataRow red in tabela.Rows){
                String ime = red["ime"].ToString();
                String prezime = red["prezime"].ToString();
                String jmbg = red["jmbg"].ToString();
                String pasos = red["pasos"].ToString();
                String broj = red["broj_telefona"].ToString();
                String mail = red["mail"].ToString();
                int grad =(int) red["idgrad"];

                textBox1.Text = ime;
                textBox2.Text = prezime;
                textBox3.Text = jmbg;
                textBox4.Text = pasos;
                textBox5.Text = broj;
                textBox6.Text = mail;
                comboBox1.SelectedValue = grad;

            }
            

        }
        private void popunaKlijentima()
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("select ime+''+prezime as ime,idkorisnik from Korisnik", konekcija);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            comboBox2.DataSource = tabela;
            comboBox2.DisplayMember = "ime";
            comboBox2.ValueMember = "idkorisnik";
            konekcija.Close();

        }
        private void popuniGradove()
        {
            SqlConnection kon = new SqlConnection(Konekcija.kon);
            kon.Open();
            SqlCommand komanda = new SqlCommand("Select * from grad", kon);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            comboBox1.DataSource = tabela;
            comboBox1.DisplayMember = "naziv_grada";
            comboBox1.ValueMember = "idgrad";
            kon.Close();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("update Korisnik set ime=@ime,prezime=@prezime,jmbg=@jmbg,pasos=@pasos,broj_telefona=@broj_telefona,mail=@mail,idgrad=@idgrad where idkorisnik=@id", konekcija);
            komanda.Parameters.AddWithValue("@ime", textBox1.Text);
            komanda.Parameters.AddWithValue("@prezime", textBox2.Text);
            komanda.Parameters.AddWithValue("@jmbg", textBox3.Text);
            komanda.Parameters.AddWithValue("@pasos", textBox4.Text);
            komanda.Parameters.AddWithValue("@broj_telefona", textBox5.Text);
            komanda.Parameters.AddWithValue("@mail", textBox6.Text);
            komanda.Parameters.AddWithValue("@idgrad", comboBox1.SelectedValue);
            komanda.Parameters.AddWithValue("@id", comboBox2.SelectedValue);
            komanda.ExecuteNonQuery();
            konekcija.Close();
            MessageBox.Show("Uspesno ste izmenili klijenta!");


        }
    }
}
