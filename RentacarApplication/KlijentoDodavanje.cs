﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentacarApplication
{
    public partial class KlijentoDodavanje : Form
    {
        public KlijentoDodavanje()
        {
            InitializeComponent();
            popunicombobox1();

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection kon = new SqlConnection(Konekcija.kon);
            kon.Open();
            SqlCommand komanda = new SqlCommand("insert into korisnik(ime,prezime,jmbg,pasos,broj_telefona,mail,idgrad) values (@ime,@prezime,@jmbg,@pasos,@broj_telefona,@mail,@idgrad)" ,kon);
            komanda.Parameters.AddWithValue("@ime", textBox1.Text);
            komanda.Parameters.AddWithValue("@prezime", textBox2.Text);
            komanda.Parameters.AddWithValue("@jmbg", textBox3.Text);
            komanda.Parameters.AddWithValue("@pasos", textBox4.Text);
            komanda.Parameters.AddWithValue("@broj_telefona", textBox5.Text);
            komanda.Parameters.AddWithValue("@mail", textBox6.Text);
            komanda.Parameters.AddWithValue("@idgrad", comboBox1.SelectedValue);
            komanda.ExecuteNonQuery();
            MessageBox.Show("Dodali ste novog klijenta.");
            kon.Close();
        }
        public void popunicombobox1()

        {
            SqlConnection kon = new SqlConnection(Konekcija.kon);
            kon.Open();
            SqlCommand komanda = new SqlCommand("Select * from grad", kon);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            comboBox1.DataSource = tabela;
            comboBox1.DisplayMember = "naziv_grada";
            comboBox1.ValueMember = "idgrad";
            kon.Close();



        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            MessageBox.Show("Odustali ste od dodavanja klijenta!");
        }
    }
}
