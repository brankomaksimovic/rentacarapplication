﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentacarApplication
{
    public partial class Marke_automobila : Form
    {
        public Marke_automobila()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection kon = new SqlConnection(Konekcija.kon);
            kon.Open();
            SqlCommand komanda = new SqlCommand("insert into marka(naziv) values (@naziv)", kon);
            komanda.Parameters.AddWithValue("@naziv", textBox1.Text);
            komanda.ExecuteNonQuery();
            MessageBox.Show("Dodali ste novu marku.");
            kon.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            MessageBox.Show("Odustali ste od unosenja marke!");
        }
    }
}
