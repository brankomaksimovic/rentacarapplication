﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentacarApplication
{
    public partial class IzmenaMarke : Form
    {
        public IzmenaMarke()
        {
            InitializeComponent();
            popuniCombobox();
            textBox1.Text = "";


        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox1.Text = comboBox1.Text;
        }
        private void popuniCombobox()
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("select * from Marka",konekcija);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            comboBox1.DataSource = tabela;
            comboBox1.DisplayMember = "naziv";
            comboBox1.ValueMember = "idmarka";
            konekcija.Close();


        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("update Marka set naziv=@nazivmarke where idmarka=@id", konekcija);
            komanda.Parameters.AddWithValue("@nazivmarke", textBox1.Text);
            komanda.Parameters.AddWithValue("@id", comboBox1.SelectedValue);
            komanda.ExecuteNonQuery();
            konekcija.Close();
            MessageBox.Show("Uspesno ste izmenili marku!");
            popuniCombobox();
        }
    }
}
