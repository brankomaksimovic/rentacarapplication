﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentacarApplication
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            popuniKlijente();
            popuniVozila();
            popuniRezervacije();
            napunidataGridKlijenti();
            napunidataGridVozila();






        }


        #region tab Klijenti

        public void popuniKlijente()
        {
            SqlConnection kon = new SqlConnection(Konekcija.kon);
            kon.Open();
            SqlCommand komanda = new SqlCommand("Select k.idkorisnik,k.ime,k.prezime,k.jmbg,k.mail,k.pasos,g.naziv_grada from Korisnik as k inner join Grad as g on k.idgrad=g.idgrad", kon);
            DataTable tabela = new DataTable();
            SqlDataAdapter adap = new SqlDataAdapter(komanda);
            adap.Fill(tabela);
            dataGridView1.DataSource = tabela;
            kon.Close();

        }




        #endregion

        #region tab Vozila

        public void popuniVozila()
        {
            SqlConnection kon = new SqlConnection(Konekcija.kon);
            kon.Open();
            SqlCommand komanda = new SqlCommand("select a.naziv_automobila,m.naziv,a.registracija,a.broj_konja,a.boja    from automobili as a inner join Marka as m on a.idmarka=m.idmarka", kon);
            DataTable tabela = new DataTable();
            SqlDataAdapter adap = new SqlDataAdapter(komanda);
            adap.Fill(tabela);
            dataGridView2.DataSource = tabela;
            kon.Close();

        }

        #endregion

        #region tab Rezervacije

        public void popuniRezervacije()
        {
            SqlConnection kon = new SqlConnection(Konekcija.kon);
            kon.Open();
            SqlCommand komanda = new SqlCommand("select k.ime,k.prezime,a.naziv_automobila,m.naziv,a.registracija,r.rezervacija_od,r.rezervacija_do from Korisnik as k inner join Rezervacija as r on k.idkorisnik=r.idkorisnik inner join Automobili as a on a.idautomobili=r.idautomobil inner join Marka as m on a.idmarka=m.idmarka", kon);
            DataTable tabela = new DataTable();
            SqlDataAdapter adap = new SqlDataAdapter(komanda);
            adap.Fill(tabela);
            dataGridView3.DataSource = tabela;
            kon.Close();


        }



        #endregion

        private void dodavanjeVozilaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Automobili frmauto = new Automobili();
            frmauto.Show();

        }

        private void dodavanjeKlijentaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            KlijentoDodavanje kln = new KlijentoDodavanje();
            kln.Show();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            popuniVozila();

        }

        private void dodavanjeMarkiAutomobilaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Marke_automobila mark = new Marke_automobila();
            mark.Show();



        }

        private void button2_Click(object sender, EventArgs e)
        {
            popuniKlijente();

        }

        private void dodajRezervacijuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Rezervacije rez = new Rezervacije();
            rez.Show();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            popuniRezervacije();
        }

        private void izmenaKlijentaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IzmenaKlijenta kli = new IzmenaKlijenta();
            kli.Show();

        }

        private void izmenaVozilaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            izmenaVozila izm = new izmenaVozila();
            izm.Show();

        }

        private void izmenaMarkiAutomobilaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IzmenaMarke izmena = new IzmenaMarke();
            izmena.Show();

        }

        private void obrisiRezervacijuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IzbrisiRezervaciju izbrisi = new IzbrisiRezervaciju();
            izbrisi.Show();

        }

        private void brisanjeKlijenataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Izbrisi_klijenta klin = new Izbrisi_klijenta();
            klin.Show();

        }

        private void brisanjeVozilaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IzbrisiVozilo izbrisi = new IzbrisiVozilo();
            izbrisi.Show();

        }

        private void izbrisiMarkuAutomobilaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IzbrisiMarku izbrisi = new IzbrisiMarku();
            izbrisi.Show();

        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)


        {
            int idklijent = (int) dataGridView1.CurrentRow.Cells[0].Value;
            RezervacijaKlijenta kli = new RezervacijaKlijenta(idklijent);
            kli.Show();

        }
        private void napunidataGridKlijenti()
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("dajSveKlijente",konekcija);
            komanda.CommandType = CommandType.StoredProcedure;
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            dataGridView4.DataSource = tabela;

            
        }
        private void napunidataGridVozila()
        {
            SqlConnection kon = new SqlConnection(Konekcija.kon);
            kon.Open();
            SqlCommand komanda = new SqlCommand("select a.naziv_automobila,m.naziv,a.registracija,a.broj_konja,a.boja    from automobili as a inner join Marka as m on a.idmarka=m.idmarka", kon);
            DataTable tabela = new DataTable();
            SqlDataAdapter adap = new SqlDataAdapter(komanda);
            adap.Fill(tabela);
            dataGridView5.DataSource = tabela;
            kon.Close();
        }
    }
}
