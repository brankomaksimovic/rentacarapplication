﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentacarApplication
{
    public partial class Rezervacije : Form
    {
        public Rezervacije()
        {
            InitializeComponent();
            popuniKlijente();
            popuniAutomobile();
            popuniGradove();
            




        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void popuniKlijente()
        {
            SqlConnection kon = new SqlConnection(Konekcija.kon);
            kon.Open();
            SqlCommand komanda = new SqlCommand("select ime+''+prezime as ime,idkorisnik from korisnik",kon);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            comboBox1.DataSource = tabela;
            comboBox1.DisplayMember = "ime";
            comboBox1.ValueMember = "idkorisnik";
            kon.Close();

        }
        private void popuniAutomobile()
        {
            SqlConnection kon = new SqlConnection(Konekcija.kon);
            kon.Open();
            SqlCommand komanda = new SqlCommand("select a.naziv_automobila+''+m.naziv as naziv,a.idautomobili from Automobili as a inner join marka as m on m.idmarka=a.idmarka", kon);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            comboBox2.DataSource = tabela;
            comboBox2.DisplayMember = "naziv";
            comboBox2.ValueMember = "idautomobili";
            kon.Close();
        }

        private void popuniGradove()
        {
            SqlConnection kon = new SqlConnection(Konekcija.kon);
            kon.Open();
            SqlCommand komanda = new SqlCommand("Select * from grad", kon);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            comboBox3.DataSource = tabela;
            comboBox3.DisplayMember = "naziv_grada";
            comboBox3.ValueMember = "idgrad";
            kon.Close();

        }

        private void Dodaj_Click(object sender, EventArgs e)
        {
            SqlConnection kon = new SqlConnection(Konekcija.kon);
            kon.Open();
            SqlCommand komanda = new SqlCommand("insert into Rezervacija(idautomobil,idgrad,rezervacija_od,rezervacija_do,idkorisnik) values(@automobil,@grad,@rezervacijaod,@rezervacijado,@korisnik)",kon);
            komanda.Parameters.AddWithValue("@automobil", comboBox2.SelectedValue);
            komanda.Parameters.AddWithValue("@korisnik", comboBox1.SelectedValue);
            komanda.Parameters.AddWithValue("@grad", comboBox3.SelectedValue);
            komanda.Parameters.AddWithValue("@rezervacijaod", dateTimePicker1.Value);
            komanda.Parameters.AddWithValue("@rezervacijado", dateTimePicker2.Value);
            komanda.ExecuteNonQuery();
            MessageBox.Show("Uspesno ste uneli rezervaciju.");
            kon.Close();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            KlijentoDodavanje dodavanje = new KlijentoDodavanje();
            dodavanje.Show();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            popuniKlijente();

        }

        private void Odustani_Click(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 1;
            MessageBox.Show("Odutali ste od unosenja rezervacije!");


        }
    }
}
