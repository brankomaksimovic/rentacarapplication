﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentacarApplication
{
    public partial class IzbrisiMarku : Form
    {
        public IzbrisiMarku()
        {
            InitializeComponent();
            popuniCombobox();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("delete from Marka where idmarka=@marka", konekcija);
            komanda.Parameters.AddWithValue("@marka", comboBox1.SelectedValue);
            komanda.ExecuteNonQuery();
            MessageBox.Show("Uspesno ste obrisali marku automobila!");
            konekcija.Close();
            popuniCombobox();
        }

       public void popuniCombobox()
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.kon);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("select idmarka,naziv from marka", konekcija);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            comboBox1.DataSource = tabela;
            comboBox1.DisplayMember = "naziv";
            comboBox1.ValueMember = "idmarka";
            konekcija.Close();



        }
    }
}
